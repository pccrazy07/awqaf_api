const jwt   = require('jsonwebtoken');
const config = require('./configurations/config');


module.exports.GenerateToken = function (payload) {  

    let token = jwt.sign(payload, config.secret, {
        expiresIn: 43200 // one month
    });

    return token;
} 

function verifyJWTToken(token) 
{
  return new Promise((resolve, reject) =>
  {
    jwt.verify(token, config.secret, (err, decodedToken) => 
    {
      if (err || !decodedToken)
      {
        return reject(err)
      }

      resolve(decodedToken)
    })
  })
}

module.exports.verifyJWT_MW = function(req, res, next)
{
  let token = req.headers['access-token'];
  console.log(token);
  
  verifyJWTToken(token)
    .then((decodedToken) =>
    {
      req.employee_id = decodedToken.employee_id;      
      next()
    })
    .catch((err) =>
    {
      res.status(400)
        .json({message: "Invalid auth token provided."})
    })
}

module.exports.VerifyToken = function (token) {
  console.log("i am here at VerifyToken");

    jwt.verify(token, config.secret, (err, decoded) =>{      
      console.log("i am here at VerifyToken 2");
      var decoded = jwt.decode(token, {complete: true});
      console.log(decoded.payload);
      return decoded.payload;
      });
}