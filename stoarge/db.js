var mysql = require('mysql')
const util = require('util');

var pool = mysql.createPool({
    connectionLimit: 10,
    host: '128.199.245.237',
    user: 'anwar',
    password: 'WHMl1O04fifyyO02AYP4r9Pu',
    database: 'tdreeb'
})
pool.getConnection((err, connection) => {
    if (err) {
        if (err.code === 'PROTOCOL_CONNECTION_LOST') {
            console.error('Database connection was closed.')
        }
        if (err.code === 'ER_CON_COUNT_ERROR') {
            console.error('Database has too many connections.')
        }
        if (err.code === 'ECONNREFUSED') {
            console.error('Database connection was refused.')
        }
    }
    if (connection) connection.release()
    return
})

pool.query = util.promisify(pool.query)

module.exports = pool