var express = require("express");
var router = express.Router();
var pool = require("../stoarge/db");
var sms = require("../utils/sms");
var token = require("../utils/jwt");

router.post("/generateotp", async (req, res) => {
  try {
    let employeeId = req.body.employee_id;
    let storedPhoneNumber = await pool.query(
      `SELECT phone FROM students where emp_id = ${employeeId}`
    );
    let OTP = Math.floor(100000 + Math.random() * 900000);
    // console.log(`OTP ${OTP}`)
    // let smsResult = await sms.SendSms(storedPhoneNumber[0].phone,`${OTP} رقم التحقق هو`);
    // console.log(smsResult.body);

    let smsResult = {
      body: {
        message_id: 761258,
        status: "S",
        remarks: "Message Submitted Sucessfully"
      }
    };

    if (smsResult.body.status === "S") {
      // otp sent
      let update = await pool.query(
        `UPDATE students SET sms_code = ${OTP} where emp_id = ${100};`
      );
      if (update.changedRows === 1) {
        res.send({
          success: true,
          message: "OTP SMS SENT",
          message_id: smsResult.body.message_id
        });
      }
    } else if (smsResult.body.status === "F") {
      // otp not sent
      res.send({
        success: false,
        message: "OTP SMS NOT SENT"
      });
    }
  } catch (err) {
    res.send({
      success: false,
      message: err
    });
  }
});

router.post("/verifyotp", async (req, res) => {
  let studentOtp = req.body.otp;
  let employee_id = req.body.employee_id;
  let storedOtp = await pool.query(
    `SELECT sms_code FROM students where emp_id = ${employee_id};`
  );

  if (storedOtp[0].sms_code == studentOtp) {
    res.send({
      sucess: true,
      message: "OTP Verefied"
    });
  } else {
    res.send({
      sucess: false,
      message: "OTP is not Verefied"
    });
  }
});

router.post("/setnewpassword", async (req, res) => {
  let employeeId = req.body.employee_id;
  let studentNewPassword = req.body.newpassword;
  let newToken = token.GenerateToken({
    employee_id: employeeId
  });

  let updateUserPassword = await pool.query(
    "UPDATE students SET password =  ?, token = ? WHERE emp_id = ?",
    [studentNewPassword, newToken, employeeId]
  );

  if (updateUserPassword.changedRows == 1) {
    res.send({
      success: true,
      token: newToken,
      message: "Token Generated"
    });
  } else {
    res.send({
      success: false,
      message: "Token Not Generated"
    });
  }
});

router.get("/profile", async (req, res) => {});

router.get("/welcome", async (req, res) => {
  res.send({
    success: true,
    message: "API is Working"
  });
});

module.exports = router;
